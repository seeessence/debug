var util = require('util');

var names = {};
var callers = {};

module.exports = function (stream, name, callee, level) {
    
    if (!process.env.npm_package_config_debug) {
        return stream;
    }
    
    if (!name && callee) {
        
        var caller = callee.caller.toString();
        
        callers[caller] = (callers[caller] || caller).replace(
            module.exports.regexp,
            function () { name = arguments[1]; }
        );
        
        name += ++names[name] || !(names[name] = 1) || '';
    }
    
    stream.name = name;
    
    for (var event in module.exports.events)
        stream.on(event, module.exports.events[event]);
};

module.exports.regexp = /([^\s\(\+\;]+)\s*\=\s*(stream)\s*\(/;

module.exports.events = {
    end:        console.log.bind(null, this.name + ' ended'),
    close:      console.log.bind(null, this.name + ' closed'),
    pause:      console.log.bind(null, this.name + ' paused'),
    drain:      console.log.bind(null, this.name + ' drained'),
    resume:     console.log.bind(null, this.name + ' resumed'),
    finish:     console.log.bind(null, this.name + ' finished'),
    readable:   console.log.bind(null, this.name + ' is readable'),
    data:       function (data) { 
                    console.log(this.name + ' data is ' + util.inspect(data));
                },
    pipe:       function (source) {
                    console.log(source.name + ' piped to ' + this.name);
                },
    unpipe:     function (destination) {
                    console.log(destination.name + ' unpiped from ' + this.name);
                }
};
